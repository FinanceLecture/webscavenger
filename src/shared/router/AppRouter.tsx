import axios from 'axios';
import React from 'react';
import { Redirect, Route, RouteComponentProps, Switch, withRouter } from 'react-router';
import AccountManagement from '../../Account/AccountManagement';
import ShowAccount from '../../Account/AccountManagement';
import Login from '../../Account/Login';
import Register from '../../Account/Register';
import Documentation from '../../ScavengerHome/Documentation';
import Download from '../../ScavengerHome/Download';
import Home from '../../ScavengerHome/Home';
import AccountGuard from '../routeProtectors/AccountGuard';
import History from '../../ScavengerHome/History';

class AppRouter extends React.Component<RouteComponentProps> {
    state = {
        permission: false,
    }

    check_token = async () => {
        const response = await axios.post("/api/check_token", {}, {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem("access_token")
                }
        })
        .then(resp => {
            if (resp.status == 200) {
                return true;
            } else {
                throw new Error("Invalid token");
            }
        })
        .catch((err) => {
                return false;
        });

        console.log("result",response)
        this.setState({ permission: response})
        console.log("permission", this.state.permission)
        return response;
    }

    logout = () => {
        axios.delete("/api/logout", { 
            headers: {
                Authorization: "Bearer "+ localStorage.getItem("access_token")
            }
        })
        .then(response => {
            this.props.history.push("/login")
        })
        .catch(err => {
            console.log(err);
        })
    }
    

    render(): React.ReactElement{
        return(
            <Switch>
            <div>
                <Route
                    path="/testing"
                    exact
                    component={ShowAccount} />
                <Route
                    path="/login"
                    exact 
                    component={Login}/>
                <Route
                    path="/register"
                    exact 
                    component={Register}/>
                <Route
                    path="/home"
                    exact 
                    render={() => (<AccountGuard>
                        <Home logout={this.logout}/>
                        </AccountGuard>)}/>
                <Route
                    path="/download"
                    exact
                    render={() => (<AccountGuard>
                        <Download logout={this.logout}/>
                        </AccountGuard>)}/>
                <Route
                    path="/docs"
                    exact 
                    render={() => (<AccountGuard>
                        <Documentation logout={this.logout}/>
                        </AccountGuard>)}/>
                 <Route
                    path="/history"
                    exact 
                    render={() => (<AccountGuard>
                        <History logout={this.logout}/>
                        </AccountGuard>)}/>
            </div>
        </Switch>
        );
    }
}

export default withRouter(AppRouter);