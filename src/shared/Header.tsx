import React from 'react';
import { Link } from 'react-router-dom';

const TITLE_OF_APPLICATION="Web-Scavenger"

const Header = (): React.ReactElement => {
    const ul_style = {
        listStyle: "none",
        margin: "0",
        padding: "0",
    }

    const li_style = {
        display: "inline-block",
        margin: "2%"
    }
    
    return (
    <div>
        <h1>{TITLE_OF_APPLICATION}</h1>
        <header>
            <nav>
                <ul style={ul_style}>
                    <li style={li_style}><Link to="/login">Login</Link></li>
                    <li style={li_style}><Link to="/register">Register</Link></li>
                    <li style={li_style}><Link to="/download">Download</Link></li>
                    <li style={li_style}><Link to="/home">Home</Link></li>
                    <li style={li_style}><Link to="/docs">Documentation</Link></li>
                    <li style={li_style}><Link to="/history">History</Link></li>
                </ul>
            </nav>
        </header>
    </div>
    );

}

export default Header;