import { Color } from '@material-ui/lab/Alert/Alert';
import React from 'react';

type Movie = {
    id: number,
    title: string,
    rating: number,
};


type NewMovie = {
    title: string,
    rating: number,
};

type HistoryEvent = {
    message: string,
    severity: Color,
    time: Date,
}

export type { Movie, NewMovie, HistoryEvent };