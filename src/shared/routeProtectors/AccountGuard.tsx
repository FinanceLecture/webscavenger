import { ReactComponent } from "*.svg";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { Redirect, RouteComponentProps, useHistory, withRouter } from "react-router-dom";
class AccountGuard extends React.Component<RouteComponentProps> {
    state = {
        permission: "false",
    }

    check_token = async () => {
        const response = await axios.post("/api/check_token", {}, {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem("access_token")
                }
        })
        .then(resp => {
            if (resp.status == 200) {
                return "true";
            } else {
                throw new Error("Invalid token");
            }
        })
        .catch((err) => {
                return "false";
        });

        console.log("result",response)
        this.setState({ permission: response})
        console.log("permission", this.state.permission)
        return response;
    }


    siteNotAvailable = () => {
        setTimeout(() => {if(this.state.permission == "false"){this.props.history.push("/login")}}, 2000)
        return(<div>
                <h1>Not Available</h1>
                <p>Login properly</p>
            </div>)
    }

    componentDidMount(){
        this.check_token()
    }


    render(){
        return(
            <div>
                {this.state.permission === "true" ? this.props.children : this.siteNotAvailable()}
            </div>
        )
    }

};

export default withRouter(AccountGuard);
