import React, {useState, useEffect, ReactElement} from 'react';
import axios from 'axios';

import { Movie, NewMovie} from '../shared/models/Movie'

const ShowAccount = (): React.ReactElement => {
    const [movies, setMovies] = useState([])
    const [newMovie, setNewMovie] = useState("")

    // useEffect(() => {
    //     axios.get("http://localhost:5000/movies")
    //         .then(response => {
    //             console.log("GetMovies", response);
    //             setMovies(response.data)
    //         })
    //         .catch(err => {
    //             console.log(err)
    //         })
    // })

    function getMovies():void {
        axios.get("/api/movies")
             .then(response => {
                 console.log("GetMovies", response);
                 setMovies(response.data.movies)
             })
             .catch(err => {
                 console.log(err)
             })
    } 

    function inputMovieHandler(event: React.ChangeEvent<HTMLInputElement>): void {
        console.log(event.target.value)
        setNewMovie(event.target.value)
    }

    function addMovie():void{
        console.log(newMovie)
        const movie: NewMovie = {
            title: newMovie,
            rating: 5,
        }

        axios.post('/api/add_movie', movie)
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
    }

    const style = {
        backgroundColor: 'red',
        cursor: 'pointer'
    }

    return(
        <div>
            <p>Home</p>
            <button style={style} onClick={() => getMovies()}>Press me</button>
            {movies.length > 0  ? movies.map((movie: Movie): React.ReactElement => {
                console.log(movie);
                return(
                    <p key={movie.title}>{movie.title}</p>
                );
            }): console.log(movies)}
            <label>Input ip-addresse</label>
            <input 
                type='text'
                onChange={inputMovieHandler} />
            <button style={{ margin: '5px'}} onClick={addMovie}>Add new Device</button>
            <button style={{ margin: '5px'}}>Start</button>
            <button style={{ margin: '5px'}}>Stop</button>
            <button style={{ margin: '5px'}}>Analyze</button>
        </div>
    )
};

export default ShowAccount;


