import React, { useState } from 'react';
import { TextField, Card, CardHeader, CardContent, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import { Link, useHistory } from 'react-router-dom';
import axios from 'axios';

const CARDWIDTH = '350px';

const useStyles = makeStyles({
    card: {
        width: CARDWIDTH,
        height: '540px',
        margin: 'auto',
    },
    cardHeader: {
        textAlign: 'left',
        color: 'white',
        background: '#000099',
        fontWeight: 'bold',
        margin: '5px'
    },
    textFields: {
        margin: '10px',
        width: "94%"
    },
    label: {
        textAlign: "left",
        margin: '10px'
    },
    button: {
        display: "inline-block",
        margin: "5%" 
    },
    error: {
        width: CARDWIDTH,
        margin: 'auto',
        marginTop: '5px',
    },
})

const Register = (): React.ReactElement => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('')
    const [passwordConfirmation, setPasswordConfirmation] = useState('')
    const [error, setError] = useState(false)
    const history = useHistory()

    const classes = useStyles();

    const onChangeUsername = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setUsername(event.target.value);
    }

    const onChangePassword = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setPassword(event.target.value);
    }

    const onChangePassConf = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setPasswordConfirmation(event.target.value);
    }

    const handleRegister = (): void => {
        axios.post("/api/register", { username: username, password: password})
             .then(response => {
                 console.log("registering", response);
                 history.push("/login");
             })
             .catch(err => {
                 console.log(err)
                 setError(true);
             })
    }
    
    return(
        <div>
            <Card className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title="Register"
                    action={
                        <Link style={{color: "#00ffff"}} to="/login">Login Page</Link>
                    }/>
                <CardContent>
                    <form>
                        <div>
                            <div className={classes.label}>
                                <label>Create a Username</label>
                            </div>
                            <TextField 
                                className={classes.textFields}
                                id="username" 
                                label="Username"
                                type="search"
                                variant="outlined" 
                                onChange={onChangeUsername}/>
                            <div className={classes.label}>
                                <label>Create a Password</label>
                            </div>
                            <TextField
                                className={classes.textFields}
                                id="password"
                                label="Password"
                                type="password"
                                autoComplete="current-password"
                                variant="outlined"
                                onChange={onChangePassword}
                                />
                            <div className={classes.label}>
                                <label>Confirm Password</label>
                            </div>
                            <TextField
                                className={classes.textFields}
                                id="passwordConfirmation"
                                label="Password"
                                type="password"
                                autoComplete="current-password"
                                variant="outlined"
                                onChange={onChangePassConf}
                                />
                            </div>
                            <div>
                                <Button 
                                    className={classes.button}
                                    variant="contained"
                                    onClick={handleRegister}
                                    disabled={password != passwordConfirmation
                                         || password.length < 6
                                         || username.length == 0
                                         || username.includes(" ")}>
                                             Register
                                </Button>
                                <Button className={classes.button} variant="contained" onClick={() => history.push("/login")}>Go to Login</Button>
                            </div>
                    </form>
                </CardContent>
            </Card>
            {error ? <Alert className={classes.error}
                severity="error">The user couldn't be registered</Alert>: null}
        </div>
    );
}

export default Register;