import React, { useState } from 'react';
import { TextField, Card, CardHeader, CardContent, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router";
import axios from 'axios';
import { Alert } from '@material-ui/lab';

const CARDWIDTH = '350px';

const useStyles = makeStyles({
    card: {
        width: '350px',
        height: '400px',
        margin: 'auto',
    },
    cardHeader: {
        textAlign: 'left',
        color: 'white',
        background: '#000099',
        fontWeight: 'bold',
        margin: '5px'
    },
    textFields: {
        margin: '10px',
        width: "94%"
    },
    label: {
        textAlign: "left",
        margin: '10px'
    },
    button: {
        display: "inline-block",
        margin: "5%" 
    },
    error: {
        width: CARDWIDTH,
        margin: 'auto',
        marginTop: '5px',
    },
})

const Login = (): React.ReactElement => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('')
    const history = useHistory();
    const classes = useStyles();

    const onChangeUsername = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setUsername(event.target.value);
    }

    const onChangePassword = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setPassword(event.target.value);
    }

    const onLogin = (): void => {
        axios.post("/api/login", { username: username, password: password })
        .then(response => {
            if(response.status == 202){
                console.log("Login successful");
                localStorage.setItem("access_token", response.data.access_token);
                localStorage.setItem("refresh_token", response.data.refresh_token);
                history.push("/home")
            }
        })
        .catch(err => {
            if(err.response.status == 406){
                setErrorMessage(err.response.data.message)
                setTimeout(() => {setErrorMessage('')}, 6000);
            }else{
                console.log(err)
            }
        })
    }

    const routeRegister = () => {
        history.push("/register");
    }
    
    return(
        <div>
            <Card className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    title="Login"
                    action={
                        <Link style={{color: "#00ffff"}} to="/register">Register Page</Link>
                    }/>
                <CardContent>
                    <form>
                        <div>
                            <div className={classes.label}>
                                <label>Username</label>
                            </div>
                            <TextField 
                                className={classes.textFields}
                                id="username" 
                                label="Username"
                                type="search"
                                variant="outlined" 
                                onChange={onChangeUsername}/>
                            <div className={classes.label}>
                                <label>Password</label>
                            </div>
                            <TextField
                                className={classes.textFields}
                                id="password"
                                label="Password"
                                type="password"
                                autoComplete="current-password"
                                variant="outlined"
                                onChange={onChangePassword}
                                />
                        </div>
                        <div>
                            <Button className={classes.button} variant="contained" onClick={onLogin}>Login</Button>
                            <Button className={classes.button} variant="contained" onClick={routeRegister}>Go to Register</Button>
                        </div>
                    </form>
                </CardContent>
            </Card>
            {errorMessage.length > 0 ? <Alert className={classes.error}
                severity="error">{errorMessage}</Alert>: null}
        </div>
    );
}

export default Login;