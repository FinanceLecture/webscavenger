import { Button, Card, makeStyles } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';

const CARDHEIGHT = "400px";

const useStyles = makeStyles({
    logout: {
        position: "absolute",
        right: "10%",
        top: "-10px",   
    },
    card: {
        width: '350px',
        height: CARDHEIGHT,
        margin: 'auto'
    },
})

const Download = (props: {
    logout:  (event: React.MouseEvent<HTMLElement>) => void
}): React.ReactElement => {
    const classes = useStyles()
    return(
        <div>
             <div style={{ position: "relative" }}> 
            <Button className={classes.logout} variant="contained" onClick={props.logout}>Logout</Button>
            <p>Download the Desktop application of the Webscavenger by clicking on the logo below</p>
            </div>
            <div>
                <Card className={classes.card}>
                    <Link style={{ margin: 'auto', marginTop: '50%' }} to="/files/desktop.zip" target="_blank" download><img style={{ width: "250px", height: "250px", margin: 'auto', marginTop: '50px' }} src='/webscavengerLogo.PNG' alt="Logo" /></Link>
                </Card>
            </div>
        </div>
    );
}

export default Download;