import { Checkbox, List, ListItem, ListItemSecondaryAction, ListItemText, makeStyles } from '@material-ui/core';
import React from 'react';
import PropTypes from "prop-types";
import axios from 'axios';


const User = (props: {
    key: number,
    device: {id: number, name: string, ip_address: string, added: boolean},
    labelId: string,
    index: number,
}) => {
    const [added, setAdded] = React.useState(props.device.added);
  
    const handleToggle = (ip_address: string) => () => {
        axios.put("/api/devices", {
            ip_address: ip_address,
            added: !added,
        }, { 
            headers: {
                Authorization: "Bearer "+ localStorage.getItem("access_token")
            }
        })
        .then(response => {
            setAdded(!added)
        })
        .catch(err => {
            console.log(err);
        })
    };
  
    return (
        <ListItem key={props.device.id} button>
            <ListItemText id={props.labelId} primary={`IP: ${props.device.ip_address}  \xa0\xa0 name: ${props.device.name}`} />
            <ListItemSecondaryAction>
            <Checkbox
                edge="end"
                onChange={handleToggle(props.device.ip_address)}
                checked={added}
                inputProps={{ 'aria-labelledby': props.labelId }}
            />
            </ListItemSecondaryAction>
        </ListItem>
    );
}

export default User;