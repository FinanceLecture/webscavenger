import { Checkbox, List, ListItem, ListItemSecondaryAction, ListItemText, makeStyles } from '@material-ui/core';
import React from 'react';
import PropTypes from "prop-types";
import axios from 'axios';


const User = (props: {
    user: {username: string, accepted: boolean},
    labelId: string,
    index: number,
}) => {
    const [checked, setChecked] = React.useState(props.user.accepted);
  
    const handleToggle = (username: string) => () => {
        axios.put("/api/register", {
            username: username,
            accepted: !checked,
        }, { 
            headers: {
                Authorization: "Bearer "+ localStorage.getItem("access_token")
            }
        })
        .then(response => {
            setChecked(!checked)
        })
        .catch(err => {
            console.log(err);
        })
    };
  
    return (
        <ListItem key={props.user.username} button>
            <ListItemText id={props.labelId} primary={`${props.index+1}) ${props.user.username}`} />
            <ListItemSecondaryAction>
            <Checkbox
                edge="end"
                onChange={handleToggle(props.user.username)}
                checked={checked}
                inputProps={{ 'aria-labelledby': props.labelId }}
            />
            </ListItemSecondaryAction>
        </ListItem>
    );
}

export default User;
