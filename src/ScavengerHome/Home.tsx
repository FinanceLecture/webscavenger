import { Button, Card, List, makeStyles, TextField } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { transpileModule } from 'typescript';
import Device from './Device';
import User from './User';

const CARDHEIGHT = "400px";
const BUTTONWIDTH = "100px";
const CARDWIDTH = '350px';

const useStyles = makeStyles({
    logout: {
        position: "absolute",
        right: "10%",
        top: "-10px",   
    },
    card: {
        width: CARDWIDTH,
        height: CARDHEIGHT,
        margin: '2%',
        overflowY: "scroll",
    },
    analyzerCard: {
        width: CARDWIDTH,
        height: CARDHEIGHT,
        margin: '2%',
    },
    wrapper: {
        display: "flex",
        justifyContent: "space-evenly",
    },
    list: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: "#FFFFF0",
        paddingBottom: "10px",
    },
    button: {
        width: BUTTONWIDTH
    },
    error: {
        width: CARDWIDTH,
        margin: 'auto',
        marginTop: '10px',
    },
})


const Home = (props: {
    logout:  (event: React.MouseEvent<HTMLElement>) => void
}): React.ReactElement => {

    const classes = useStyles();
    const [users, setUsers] = useState([]);
    const [devices, setDevices] = useState([])
    const [allowed, setAllowed] = useState("false");
    const [running, setRunning] = useState(false)
    const [message, setMessage] = useState("")
    const [error, setError] = useState(false)
    const [warning, setWarning] = useState(false)
    const [thread, setThread] = useState(0)
    const [defaultAnalyzer, setDefaultAnalyzer] = useState(true)
    const [errorAnalyzer, setErrorAnalyzer] = useState(false)
    const [messageAnalyzer, setMessageAnalyzer] = useState("")
    const [summery, setSummery] = useState("")
    const [threadAnalyzer, setThreadAnalyzer] = useState(0)

    const eraseErrorWarning = () => {
        setError(false)
        setWarning(false)
    }

    useEffect(() => {
        if(thread === 0){
            setThread(1)
            setTimeout(() => {
                setMessage("")
                setThread(0)
              }, 10000);
        }
    }, [message])

    useEffect(() => {
        if(threadAnalyzer === 0){
            setThreadAnalyzer(1)
            setTimeout(() => {
                setMessageAnalyzer("")
                setThreadAnalyzer(0)
              }, 10000);
        }
    }, [messageAnalyzer])

    useEffect(() => {
        axios.get("/api/check_role", { 
            headers: {
                Authorization: "Bearer "+ localStorage.getItem("access_token")
            }
        })
        .then(response => {
            setUsers(response.data.users);
            setAllowed(response.data.extended_rights);
            setDevices(response.data.devices)
            setRunning(response.data.running)
        })
        .catch(err => {
            console.log(err);
        })
    }, [JSON.stringify(users), JSON.stringify(devices)])

    const searchNewDevice = () => {
        eraseErrorWarning()
        axios.post("/api/search_device", {}, { 
            headers: {
                Authorization: "Bearer "+ localStorage.getItem("access_token")
            }
        })
        .then(response => {
            setDevices(response.data.devices)

            if(response.status == 202){
                setWarning(true)
            }
            
            setMessage(response.data.message)
        })
        .catch(err => {
            console.log(err);

            if(err.response.status = 406){
                setError(true)
                setMessage(err.response.data.message)
            }
        })
    }

    const startMacScavenger = () => {
        eraseErrorWarning()
        axios.post("/api/start_scavenger", {}, { 
            headers: {
                Authorization: "Bearer "+ localStorage.getItem("access_token")
            }
        })
        .then(response => {
            setMessage(response.data.message)
            setRunning(true)
        })
        .catch(err => {
            console.log(err);

            if(err.response.status == 412){
                setMessage(err.response.data.message)
                setWarning(true)
            }
        })
    }

    const stopMacScavenger = () => {
        eraseErrorWarning()
        axios.delete("/api/stop_scavenger", { 
            headers: {
                Authorization: "Bearer "+ localStorage.getItem("access_token")
            }
        })
        .then(response => {
            setMessage(response.data.message)
            setRunning(false)
        })
        .catch(err => {
            console.log(err);
            setError(true)
            setMessage('An Error occurred')
        })
    }

    const analyze = () => {
        setMessageAnalyzer("")
        setErrorAnalyzer(false)
        axios.get("/api/analyze_scavenger", { 
            params: {
                defaultAnalyzer: defaultAnalyzer
            },
            headers: {
                Authorization: "Bearer "+ localStorage.getItem("access_token")
            }
        })
        .then(response => {
            console.log(response)
            setMessageAnalyzer(response.data.message)
            setSummery(response.data.summery)
        })
        .catch(err => {
            console.log(err);
            setSummery("")
            setErrorAnalyzer(true)
            setMessageAnalyzer(err.response.data.message)
        })
    }

    

    return(
        <div>
            <div style={{ position: "relative" }}> 
                <Button className={classes.logout} variant="contained" onClick={props.logout}>Logout</Button>
                <p>Welcome to the Webscavenger! Choose the devices and start gathering data</p>
            </div>
            <div className={classes.wrapper}>       
                { users.length > 0 ? <Card className={classes.card}>
                <List dense className={classes.list}>
                    {users.map((user: {username: string, accepted: boolean}, index: number) => {
                    const labelId = `checkbox-list-secondary-label-${user.username}`;
                    return(<User key={user.username} user={user} labelId={labelId} index={index} />)
                })}
                </List>
                </Card>: null}
                <div>
                    <Card className={classes.card}>
                        <List dense className={classes.list}>
                            { devices ? devices.map((device: {id: number, name: string, ip_address: string, added: boolean}, index: number) => {
                            const labelId = `checkbox-list-secondary-label-${device.ip_address}`;
                            return(<Device key={device.id} device={device} labelId={labelId} index={index} />)
                        }): null }
                        </List>
                    </Card>
                    <div style={{ display: "flex", justifyContent: "space-between", marginLeft: "10px"}}>
                        <Button className={classes.button} variant="contained" onClick={searchNewDevice}>Search</Button>
                        <Button className={classes.button} variant="contained" disabled={running} onClick={startMacScavenger} >Start</Button>
                        <Button className={classes.button} variant="contained" disabled={!running} onClick={stopMacScavenger} >Stop</Button>
                    </div>
                    <div>
                        { message.length > 1 ?  <Alert className={classes.error} severity={warning ? "warning" : (error ? "error" : "success") }>{message}</Alert>: null }
                    </div>
                </div>
                <div>
                    <Card className={classes.analyzerCard}>
                        <TextField
                            style={{ margin: "20px", width: "300px", height: "200px" }}
                            id="outlined-multiline-static"
                            label="Result"
                            multiline
                            rows={14}
                            value={summery.length >1 ? summery : "Press Analyze"}
                            InputProps={{ disableUnderline: true }}
                            />
                    </Card>
                    <Button className={classes.button} variant="contained" onClick={analyze}>Analyze</Button>
                    { messageAnalyzer.length > 1 ?  <Alert className={classes.error} severity={errorAnalyzer ? "error" : "success"}>{messageAnalyzer}</Alert>: null }
                    <div style={{ display: "flex", justifyContent: 'space-evenly', width: CARDWIDTH, margin: "10px", borderStyle: 'solid', borderColor: "black",}}>
                        <p style={{ margin: "30px" }}>Default? Currently:{defaultAnalyzer ? "Default" : "Custom" }</p>
                        <Button style={{ marginTop: "40px", marginBottom: "40px", marginRight: "20px"}} variant="contained" disabled={defaultAnalyzer} onClick={() => setDefaultAnalyzer(true)}>Yes</Button>
                        <Button style={{ marginTop: "40px", marginBottom: "40px", marginRight: "40px"}} variant="contained" disabled={!defaultAnalyzer} onClick={() => setDefaultAnalyzer(false)}>No</Button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Home;