import { Button, IconButton, makeStyles } from '@material-ui/core';
import { pdf } from '@react-pdf/renderer';
import React, { useState } from 'react';
import { Page } from "react-pdf";
import { Document } from 'react-pdf/dist/esm/entry.webpack';
import ZoomInOutlinedIcon from '@material-ui/icons/ZoomInOutlined';
import ZoomOutOutlinedIcon from '@material-ui/icons/ZoomOutOutlined';

const useStyles = makeStyles({
  document: {
    margin: 'auto',
    backgroundColor: 'gray'
  },
  page: {
    marginTop: "50px",
    marginBottom: "100px",
    margin: 'auto',
    width: '600px',
    height: '800px',
    backgroundColor: 'gray',
  },
  divDocs : {
    width: '800px',
    margin: 'auto',
    borderStyle: 'solid',
    borderColor: "black",
    height: '700px',
    overflowY: "scroll",
    backgroundColor: 'gray',
  },
  navPdf: {
    width: '400px',
    margin: 'auto',
    marginBottom: '20px',
    display: "flex",
    justifyContent: 'space-evenly'
  },
  logout: {
    position: "absolute",
    right: "10%",
    top: "-10px",  
  },
});


const Documentation = (props: {
  logout:  (event: React.MouseEvent<HTMLElement>) => void
}): React.ReactElement => {
    const classes = useStyles()
    const [numPages, setNumPages] = useState(0)
    const [pageNumber, setPageNumber] = useState(1)
    const [scale, setScale] = useState(1)

    const onDocumentLoadSuccess = ({ numPages }:any) => {
      setNumPages( numPages );
    };


    const goToPrevPage = () => {
      setPageNumber( pageNumber - 1 );
    }
    
    const goToNextPage = () => {
      setPageNumber(pageNumber + 1);
    }

    const handleZoomIn = () => {
      if(scale < 1.3 || scale > 0.9){
        setScale(scale +0.05)
      } else {
        setScale(scale + 0.25)
      }
    }

    const handleZoomOut = () => {
      if(scale < 1.3 || scale > 0.9 ){
        setScale(scale - 0.05)
      } else {
        setScale(scale - 0.25)
      }
    }
      
    return (
      <div>
        <div style={{ position: "relative" }}>
        <nav className={classes.navPdf}>
          <Button variant="contained" disabled={pageNumber <= 1 } onClick={goToPrevPage}>Prev</Button>
          <Button variant="contained" disabled={pageNumber >= numPages } onClick={goToNextPage}>Next</Button>
          <IconButton color="primary" aria-label="upload picture" component="span" disabled={scale >= 1.5} onClick={handleZoomIn}>
            <ZoomInOutlinedIcon/>
          </IconButton>
          <IconButton color="primary" aria-label="upload picture" component="span" disabled={scale <= 0.5} onClick={handleZoomOut}>
            <ZoomOutOutlinedIcon/>
          </IconButton>
        </nav>
          <Button className={classes.logout} variant="contained" onClick={props.logout}>Logout</Button>
        </div>

        <div className={classes.divDocs}>
          <Document
            file="/files/docs.pdf"
            onLoadSuccess={onDocumentLoadSuccess}
            className={classes.document}
          >
            <Page className={classes.page} pageNumber={pageNumber} scale={scale}/>
          </Document>
        </div>

        <p>
          Page {pageNumber} of {numPages}
        </p>
      </div>
    );
}

export default Documentation;