import { Button, Card, List, ListItem, makeStyles } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { HistoryEvent, NewMovie } from "c:/Users/fabia/Desktop/HS2020/studies/web-mac-scavenger/src/shared/models/Movie"


const CARDHEIGHT = "600px";

const useStyles = makeStyles({
    logout: {
        position: "absolute",
        right: "10%",
        top: "-10px",   
    },
    card: {
        width: '400px',
        height: CARDHEIGHT,
        margin: 'auto',
        overflowY: "scroll",
    },
    list: {
        width: '100%',
        backgroundColor: "#FFFFF0",
    },
    alert: {
        width: '100%',
    }
})

const History = (props: {
    logout:  (event: React.MouseEvent<HTMLElement>) => void
}): React.ReactElement => {
    const classes=useStyles()

    const [history, setHistory] = useState([])

    const messageMapper = (message: {title: number, device: string, time: string}) => {
        console.log(message)
        const event: HistoryEvent = {
            message: "Unknown Event occurred",
            severity: "error",
            time: new Date(Date.now()),
        }

        switch(message.title){
            case 1:
                event.message="Mac-Scavenger was started"
                event.severity="success"
                event.time= new Date(Date.parse(message.time))
                return event;
            case 2:
                event.message="Mac-Scavenger was stopped"
                event.severity="success"
                event.time= new Date(Date.parse(message.time))
                return event;
            case 3:
                event.message=`Device (${message.device === null ? "undefined": message.device}) was added`
                event.severity="success"
                event.time= new Date(Date.parse(message.time))
                return event;
            case 4:
                event.message="Results were analyzed"
                event.severity="success"
                event.time= new Date(Date.parse(message.time))
                return event;
            case 5:
                event.message=`New Device (${message.device === null ? "undefined": message.device}) was found`
                event.severity="success"
                event.time= new Date(Date.parse(message.time))
                return event;
            case 6:
                event.message="No new Device was found"
                event.severity="warning"
                event.time= new Date(Date.parse(message.time))
                return event;
            case 7:
                event.message="An error occurred"
                event.severity="error"
                event.time= new Date(Date.parse(message.time))
                return event;
            default:
              return event
          }
    }
    
    useEffect(() => {
        axios.get("/api/history", { 
            headers: {
                Authorization: "Bearer "+ localStorage.getItem("access_token")
            }
        })
        .then(response => {
            console.log(response)
            setHistory(response.data.history.reverse())
        })
        .catch(err => {
            console.log(err);
        })
    }, [JSON.stringify(history)])


    return(
        <div>
            <div style={{ position: "relative" }}> 
                <Button className={classes.logout} variant="contained" onClick={props.logout}>Logout</Button>
                <p>This history shows your past actions</p>
            </div>
            <div>
                <Card className={classes.card}>
                    { history.length !== 0 ? 
                    <List component="nav" className={classes.list} aria-label="contacts">{
                        history.map((event: {title: number, device: string, time: string}, index: number) =>{
                            const convertedEvent = messageMapper(event)
                            return(
                            <ListItem key={index}>
                                <Alert className={classes.alert} severity={convertedEvent.severity}>
                                    {`${convertedEvent.time.toLocaleTimeString()}  \xa0\xa0 ${convertedEvent.message}`}</Alert>
                            </ListItem>)
                        })
                    }</List> : null}
                </Card>
            </div>
        </div>
    );
}

export default History;

