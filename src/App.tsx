import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import AppRouter from './shared/router/AppRouter';
import Header from './shared/Header';

function App() {
  return (
      <BrowserRouter>
        <div className="App">
            <Header/>
            <AppRouter/>
        </div>
      </BrowserRouter>

  );
}

export default App;
