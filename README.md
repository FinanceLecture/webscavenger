# WebScavenger

This is the implementation of the Bachelor's thesis at UZH

# Description

The WebScavenger is a tool to track mobile devices over a certain area using packages emitted by the mobile device. No GPS is required ot locate the devices. The implementation provides a easy web-interface for tracking, which additionally provides statistics over the tracked devices and enables the user to easily add new sensors.

# Abstract

Passive wireless tracking is used to track an individual with a wireless capable device over
an area covered by a network. This technology could help to monitor occupancy levels in
public transport systems and buildings. To avoid being tracked, countermeasures, such
as MAC-Randomization, have been implemented on WiFi capable devices. A thesis at
the University of Zurich analyzed these countermeasures and presented a system called
MAC-Scavenger which circumvents them.
This thesis analyses the basic background concepts of wireless networks and passive wireless tracking. It investigates the MAC-Scavenger and its data gathering process in detail
and derives requirements for an improved system. The main requirement is to simplify the
existing MAC-Scavenger and therefore to make the passive wireless tracking technology
available for non-computer experts. Another requirement, inferred from studies in related
research, includes the secure handling of the gathered data. Based on these requirements
the Web-Scavenger is implemented and documented in the scope of this thesis. The documentation shows the high level architecture as well as key implementation parts. In the
end the requirements are evaluated against the implemented system.

# To start

To start the application use:
`yarn start`

